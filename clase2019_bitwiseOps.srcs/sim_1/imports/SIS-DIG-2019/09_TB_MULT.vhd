--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
 
ENTITY TB_MULT IS
END TB_MULT;
 
ARCHITECTURE behavior OF TB_MULT IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MULTIPLIER
    PORT(
         A : IN  std_logic_vector(7 downto 0);
         --B : IN  std_logic_vector(7 downto 0);
         S : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(7 downto 0) := (others => '0');
   --signal B : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal S : std_logic_vector(7 downto 0);
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MULTIPLIER PORT MAP (
          A => A,
          --B => B,
          S => S
        );

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;
        A <= "00101000";	-- 40
		--A <= "10000000";	-- -128
		--B <= "10000000";	-- -128
      wait for 100 ns;
        A <= "11011000";      -- -40
		--A <= "10000000";	-- -128
        --B <= "01111111";    --  127
      wait for 100 ns;
        A <= "01011011";     -- 91
      
      wait for 100 ns;
        A <= "10100101";    -- -91
      
      wait for 100 ns;
        A <= "00111000";      -- 56
        
      wait for 100 ns;
        A <= "11001000";      -- -56
        
      wait for 100 ns;
        A <= "01111111";      -- 127
        
      wait for 100 ns;
        A <= "10000001";      -- -127
        
        
        

      wait;
   end process;

END;
