----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_signed.ALL;

entity MULTIPLIER is
    Port ( A : in  STD_LOGIC_VECTOR (7 downto 0);   -- Senial de 8 bits
--           B : in  STD_LOGIC_VECTOR (7  downto 0);  -- Senial de 8 bits
           S : out STD_LOGIC_VECTOR (7 downto 0)); -- Senial de 16 bits (bits A + bits B)
end MULTIPLIER;

architecture Behavioral of MULTIPLIER is
constant ROUND_VAL : std_logic_vector(16 downto 0) := (6 => '1', others => '0');
constant GAIN :      std_logic_vector(8  downto 0) := "010110010"; --1.390625
signal   DOUT_INT :  std_logic_vector(16 downto 0);
signal   MULT:       std_logic_vector(16 downto 0);	-- Agregar 1 bit (sign x unsigned)

begin

--	S <= A * B; -- (signed x signed)
    -- Si A y B son se�ales con signo (-128 a 127) el rango din�mico de S es:
    -- Valor menor de S: -128 x 127 = -16256 = 1100 0000 1000 0000 = hC080
    -- Valor mayor de S: -128 x -128 = 16384 = 0100 0000 0000 0000 = h4000
    -- S debe ser de 16 bits que incluye de -32768 a 32767
	-- S <= A * ('0' & B); 	-- (signed x unsigned)
	-- Se agrega un '0' al signed, se asegura dato siempre positivo


	MULT <= A * GAIN;  				-- XXXX XXXX * 0 1.011 0010 (17 bits = 8 + 9)
									-- DIN -91 a 91 (1010 0101 a 0101 1011)
	DOUT_INT <= MULT + ROUND_VAL;   -- 17X + 0 0000 0000 0100 0000
	S <= DOUT_INT(14 downto 7);	    -- - -111 1111 1--- ----

end Behavioral;





